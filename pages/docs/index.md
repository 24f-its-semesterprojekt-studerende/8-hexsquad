---
hide:
  - footer
  - toc
---

# UCL PBa IT Sikkerhed - Semesterprojekt gruppe XX

På dette website finder du projektplaner, læringslogs og andet relateret til semesterprojektet

Læringslogs og link til dokumentation skal inkluderes som bilag i projektrapporten (eksamensaflevering).  

Procesafsnittet i rapporten skal beskrive de enkelte ugers projektarbejde med henvisning til logs (link til relevante dele af denne gitlab side) 

Semesterprojektet er beskrevet på: [https://ucl-pba-its.gitlab.io/24f-its-semester-projekt/](https://ucl-pba-its.gitlab.io/24f-its-semester-projekt/)
